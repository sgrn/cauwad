/* CAUWAD stands for Cellular Automata Utilizing WAve Dissipation.
 * Copyright (C) 2021  Sangsoic
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <limits.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>

#include "Core/globals.h"
#include "Core/matrix.h"

static Matrix mat;

struct Option
{
	size_t m, n;
	unsigned long long N;
	char * fieldLayoutPath, * agentsLayoutPath, * snapshotsPath;
	void (* Field_display)(const VField, FILE * const, const unsigned char);
	bool printToSTDOUT;
	unsigned char precision;
};

typedef struct Option Option;

Option Option_allocinit(void)
{
	Option options = {0, 0, 0, NULL, NULL, NULL, Field_print_yx_field, true, 3};
	return options;
}

void print_help(void)
{
	puts("Usage: cauwad (-d <m-line> <n-col> | -f FIELDFILE) [-n <n-iteration>] [-a AGENTFILE] [-o SNAPSHOTPREFIX] [-p <significant-figure>] [-m (yx | x | y)] [-s] [-h]");
	puts("CAUWAD is a cellular automata simulator, capable of simulating variable emergence phenomena via Wave Propagation, only by tweaking very few parameters.");
	puts("");
	puts("Options:");
	puts("\t-d <m-line> <n-col>\t\tSpecifies the dimensions of the field. (Initializes the field with (0,0) values)");
	puts("\t-f FIELDFILE\t\t\tImports field values from FIELDFILE file.") ;
	puts("\t-n <n-iteration>\t\tSpecifies the number of iteration(s) to perform. [Default: 0]");
	puts("\t-a AGENTFILE\t\t\tImports agent(s) initial position from AGENTFILE file.");
	puts("\t-o SNAPSHOTPREFIX\t\tExports a snapshot file at SNAPSHOTPREFIX location, for each iteration.");
	puts("\t-p <significant-figure>\t\tSpecifies the number of figures after the decimal point to take into account. [Default: 3]");
	puts("\t-m (yx | x | y)\t\t\tSpecifies what coordinate(s) to print out from each field cell. [Default: yx]");
	puts("\t-s\t\t\t\tActivates silent mode, nothing will be print out to stdout. (faster)");
	puts("\t-h\t\t\t\tShows help menu.");
	puts("");
	puts("Exit status:");
	puts("\t0\tmeans execution went fine.");
	puts("\t1\tmeans an error occurred during execution.");
	puts("");
	puts("For more documentation on how CAUWAD works, go to https://gitlab.com/sangsoic/cauwad webpage or simply read the README.md instructions.");
	exit(EXIT_SUCCESS);
}

bool validated_precision(const char * const strPrecision, unsigned char * const precision)
{
	bool exitStatus;
	unsigned long tmpPre;
	char * end;
	exitStatus = true;
	tmpPre = strtoul(strPrecision, &end, 10);
	if ((*end == '\0') && (errno != ERANGE)) {
		exitStatus = tmpPre > 6;
		if (exitStatus) {
			fprintf(stderr, "cauwad error: given precision is out of range, must be an integer within [0;6] interval  (type option -h for help)\n");
		} else {
			*precision = (unsigned char)tmpPre;
		}
	} else {
		fprintf(stderr, "cauwad error: given '%s' is not a valid precision  (type option -h for help)\n", strPrecision);
	}
	return exitStatus;
}

bool validated_displaymode(const char * const displayOption, void (* * Field_display)(const VField, FILE * const, const unsigned char))
{
	bool exitStatus;
	exitStatus = false;
	if (strcmp(displayOption, "x") == 0) {
		*Field_display = Field_print_x_field;
	} else if (strcmp(displayOption, "y") == 0) {
		*Field_display = Field_print_y_field;
	} else if (strcmp(displayOption, "yx") == 0) {
		*Field_display = Field_print_yx_field;
	} else {
		fprintf(stderr, "cauwad error: given '%s' is not a valid display argument, must be one of (yx | y | x) argument   (type option -h for help)\n", displayOption);
		exitStatus = true;
	}
	return exitStatus;
}

#define UINTEGER_VALIDATER(NAME, CONVERTER, TYPE, MAXVAL) \
	bool validated_##NAME(const char * const struint, TYPE * const val) \
	{ \
		bool exitStatus; \
		char * end; \
		exitStatus = true; \
		*val = CONVERTER(struint, &end, 10); \
		if (*end == '\0') { \
			exitStatus = errno == ERANGE; \
			if (exitStatus) { \
				fprintf(stderr, "cauwad error: given %s is too big, max value is %llu (type option -h for help)\n", struint, MAXVAL); \
			} \
		} else { \
			fprintf(stderr, "cauwad error: given '%s' is not a valid unsigned integer  (type option -h for help)\n", struint); \
		} \
		return exitStatus; \
	}

UINTEGER_VALIDATER(ulint, strtoul, unsigned long, ULONG_MAX)

UINTEGER_VALIDATER(ullint, strtoull, unsigned long long, ULLONG_MAX)

bool validate_dimensions(const int argc, char ** const argv, const int opt, size_t * const optionM, size_t * const optionN)
{
	bool optionIsInvalid;
	optionIsInvalid = true;
	if ((optind < argc) && (argv[optind][0] != '-')) {
		optionIsInvalid = validated_ulint(optarg, optionM) || validated_ulint(argv[optind], optionN);
		optind += 1;
	} else {
		fprintf(stderr, "cauwad error: missing argument for option: -%c  (type option -h for help)\n", opt);
	}
	return optionIsInvalid;
}

bool enough_to_run_cauwad(int argc, char * * argv, const const Option options, const bool currentOptionValidityStatus)
{
	bool optionIsInvalid;
	optionIsInvalid = true;
	if (optind == argc) {
		if ((options.m != 0) || (options.fieldLayoutPath != NULL)) {
			optionIsInvalid = currentOptionValidityStatus;
		} else {
			fprintf(stderr, "cauwad error: missing either -d or -f option  (type option -h for help)\n");
		}
	} else {
		fprintf(stderr, "cauwad error: invalid argument: %s  (type option -h for help)\n", argv[optind]);
	}
	return optionIsInvalid;
}

#define PARAMETERS ":n:d:f:a:o:m:sp:h"

Option get_options(int argc, char * * argv)
{
	Option options;
	bool optionIsInvalid;
	int opt;
	options = Option_allocinit();
	opt = getopt(argc, argv, PARAMETERS);
	optionIsInvalid = false;
	while (! ((opt == -1) || optionIsInvalid)) {
		switch (opt) {
			case 's' :
				options.printToSTDOUT = false;
				break;
			case 'h' :
				print_help();
				break;
			case 'p' :
				optionIsInvalid = validated_precision(optarg, &options.precision);
				break;
			case 'n' :
				optionIsInvalid = validated_ullint(optarg, &options.N);
				break;
			case 'o' :
				options.snapshotsPath = optarg;
				break;
			case 'm' :
				optionIsInvalid = validated_displaymode(optarg, &options.Field_display);
				break;
			case 'd' :
				optionIsInvalid = validate_dimensions(argc, argv, opt, &options.m, &options.n);
				break;
			case 'f' :
				options.fieldLayoutPath = optarg;
				break;
			case 'a' :
				options.agentsLayoutPath = optarg;
				break;
			case ':' :
				fprintf(stderr, "cauwad error: missing argument for option: -%c  (type option -h for help)\n", optopt);
				optionIsInvalid = true;
				break;
			case '?' :
				fprintf(stderr, "cauwad error: invalid option: -%c  (type option -h for help)\n", optopt);
				optionIsInvalid = true;
				break;
		}
		opt = getopt(argc, argv, PARAMETERS);
	}
	optionIsInvalid = enough_to_run_cauwad(argc, argv, options, optionIsInvalid);
	if (optionIsInvalid) {
		exit(EXIT_FAILURE);
	}
	return options;
}

void set_signal_handler(void);

int main (int argc, char * * argv)
{
	Option options;
	set_signal_handler();
	options = get_options(argc, argv);
	mat = Matrix_allocinit(options.m, options.n, options.agentsLayoutPath, options.fieldLayoutPath);
	for (; globalInterationNumber < options.N; globalInterationNumber++) {
		Matrix_plot(mat);
		Matrix_display(mat, options.snapshotsPath, options.printToSTDOUT, options.Field_display, options.precision);
		Matrix_clear(mat);
		Matrix_refresh(mat);
	}
	Matrix_free(&mat);
	return EXIT_SUCCESS;
}

void sig_handler(int sig);

void set_signal_handler(void)
{
	int sigList[] = {SIGINT, SIGTERM, SIGQUIT, SIGHUP, SIGABRT, SIGSEGV};
	size_t i;
	i = 0;
	while ((i < 6) && signal(sigList[i], sig_handler) != SIG_ERR) {
		i++;
	}
	if (i != 6) {
		fprintf(stderr, "error: could not setup due to: %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
}

void sig_handler(int sig)
{
	psignal(sig, NULL);
	fprintf(stderr, "*FREEING MATRIX as CAUWAD received signal.*\n");
	Matrix_free(&mat);
	exit(EXIT_FAILURE);
}

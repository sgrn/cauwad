compiler = gcc
objects  = main.o \
globals.o agent.o index.o matrix.o velocity.o wave.o \
shared-velocity.o shared-agent.o\
vector.o list.o queue.o

cauwad : $(objects)
	$(compiler) -o cauwad $(objects) -lm

main.o : main.c
	$(compiler) -c main.c

globals.o : Core/globals.c Core/globals.h
	$(compiler) -c Core/globals.c

agent.o : Core/agent.c Core/agent.h
	$(compiler) -c Core/agent.c

index.o : Core/index.c Core/index.h
	$(compiler) -c Core/index.c

matrix.o : Core/matrix.c Core/matrix.h
	$(compiler) -c Core/matrix.c

velocity.o : Core/velocity.c Core/velocity.h
	$(compiler) -c Core/velocity.c

wave.o : Core/wave.c Core/wave.h
	$(compiler) -c Core/wave.c

shared-velocity.o : SharedLib/shared-velocity.c SharedLib/shared-velocity.h
	$(compiler) -c SharedLib/shared-velocity.c

shared-agent.o : SharedLib/shared-agent.c SharedLib/shared-agent.h
	$(compiler) -c SharedLib/shared-agent.c

vector.o : Lib/Vector/vector.c Lib/Vector/vector.h
	$(compiler) -c Lib/Vector/vector.c

list.o : Lib/List/list.c Lib/List/list.h
	$(compiler) -c Lib/List/list.c

queue.o : Lib/Queue/queue.c Lib/Queue/queue.h
	$(compiler) -c Lib/Queue/queue.c

.PHONY : clean
clean :
	rm cauwad $(objects)

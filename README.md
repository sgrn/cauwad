![9th iteration using the bundled default configuration](Img/example.png)

***This project has been released under the GPL-3.0-or-later license.***

***If you experience any problem, feel free to submit an issue or contact me at sangsoic@protonmail.com.***

# Principle

The output of simulation is a two-dimensional grid of vectors (also referred as cells). Each cell has a state, which can vary through time depending on the position and type of agents. An agent is an entity with only three elementary properties:

* type, which can be any mathematical sequence S : N^2 x N^2 x N --> R x R.
* position in the field.

Each iteration of the simulation can either be printed out on stdout or stored chronologically in ordered snapshot files. *(see help)*

# To Compile

run `make`

# To Execute

run `./cauwad [options]`

# Help

You can invoke the help menu by running `./cauwad -h`. A list of all options, arguments and short descriptions will be displayed.

# Tweaking

Tweaking the simulation can be achieved by :

* Changing the *number*, *type* and *initial position* of agents via the input agent file.
* Changing the *dimension* and *initial vector values* of the field via the input field file.
* Changing the code in between the ML_SCRIPT tokens in the **shared library** files.

Examples of code are available in the InputLayout/ directory and in the SharedLib/ directory.

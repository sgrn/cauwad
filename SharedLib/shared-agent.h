/* CAUWAD stands for Cellular Automata Utilizing WAve Dissipation.
 * Copyright (C) 2021  Sangsoic
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef SHARED_AGENT_H
	#define SHARED_AGENT_H

	#include <limits.h>

	#include "../Core/velocity.h"
	#include "../Core/wave.h"
	#include "../Core/agent.h"
	#include "../Core/index.h"
	#include "../Core/globals.h"

	#include "../Lib/Vector/vector.h"

	ACell ACell_next_agent(const ACell currentAgent, const VField field);
	void init_sequence_table(void);

#endif

/* CAUWAD stands for Cellular Automata Utilizing WAve Dissipation.
 * Copyright (C) 2021  Sangsoic
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "shared-velocity.h"

VECTOR_GET(VCell, VCell)

VECTOR_SET(VCell, VCell)

void Field_apply_wave(const VField field, const size_t linearIndices, const Index where, const Wave wave)
{
	VCell currentVector, newVector;
	Sequence sequence;
	sequence = wave.sequence;
	currentVector = VectorVCell_get(field.field, linearIndices);

	// MLSCRIPT-FIELD: Determines what happens when two field vectors collide.
	newVector.x = currentVector.x + sequence.X(wave.center, where, wave.radius);
	newVector.y = currentVector.y + sequence.Y(wave.center, where, wave.radius);
	// END

	VectorVCell_set(field.field, linearIndices, newVector);
}

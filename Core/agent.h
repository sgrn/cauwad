/* CAUWAD stands for Cellular Automata Utilizing WAve Dissipation.
 * Copyright (C) 2021  Sangsoic
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef AGENT_H
	#define AGENT_H

	#include <stdbool.h>
	#include <string.h>
	#include <errno.h>
	#include <ctype.h>

	#include "index.h"
	#include "wave.h"
	#include "velocity.h"

	#include "../Lib/List/list.h"

	struct AgentCell
	{
		unsigned char type;
		Index position;
		Sequence sequence;
		struct QueueWave * currEmission;
	};

	typedef struct AgentCell ACell;

	#include "../SharedLib/shared-agent.h"

	LIST(ACell, ACell)

	/**
	 * \fn bool is_position_in_field(const size_t m, const size_t n, const Index position)
	 * \brief Tests if given position is in the field.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 */
	bool is_position_in_field(const size_t m, const size_t n, const Index position);

	/**
	 * \fn bool ListACell_add_agent(ListACell * const agents, ListStructQWave * const structQWaves, const unsigned char type, const Index position, const Sequence sequence, const size_t m, const size_t n)
	 * \brief Adds new agent.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 */
	bool ListACell_add_agent(ListACell * const agents, ListStructQWave * const structQWaves, const unsigned char type, const Index position, const Sequence sequence, const size_t m, const size_t n);

	/**
	 * \fn bool ListACell_init(ListACell * const agents, ListStructQWave * const structQWaves, const char * const initLayoutName, const size_t m, const size_t n)
	 * \brief Initializes agent list.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 */
	bool ListACell_init(ListACell * const agents, ListStructQWave * const structQWaves, const char * const initLayoutName, const size_t m, const size_t n);

	/**
	 * \fn bool ACell_change_agent(ACell * const agent, ListStructQWave * const structQWaves, const ACell newAgent, const size_t m, const size_t n)
	 * \brief Alters agent status (position, wave sequence, emission state).
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 */
	bool ACell_change_agent(ACell * const agent, ListStructQWave * const structQWaves, const ACell newAgent, const size_t m, const size_t n);

	/**
	 * \fn bool ListACell_refresh(ListACell * const agents, ListStructQWave * const structQWaves, const VField field)
	 * \brief Refreshes agents status in the field.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 */
	bool ListACell_refresh(ListACell * const agents, ListStructQWave * const structQWaves, const VField field);

	/**
	 * \fn void ListACell_print_agents_status(ListACell * const agents, FILE * const output)
	 * \brief Prints list of all agents to stdout.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 */
	void ListACell_print_agents_status(ListACell * const agents, FILE * const output);

#endif

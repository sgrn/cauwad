/* CAUWAD stands for Cellular Automata Utilizing WAve Dissipation.
 * Copyright (C) 2021  Sangsoic
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "agent.h"

bool is_position_in_field(const size_t m, const size_t n, const Index position)
{
	return (position.i < m) && (position.j < n);
}

LIST_PUSH_BACK(ACell, ACell)

LIST_PUSH_BACK(struct QueueWave, StructQWave)

LIST_AT_TAIL(struct QueueWave, StructQWave)

bool ListACell_add_agent(ListACell * const agents, ListStructQWave * const structQWaves, const unsigned char type, const Index position, const Sequence sequence, const size_t m, const size_t n)
{
	bool positionExist;
	ACell agent;
	if (positionExist = is_position_in_field(m, n, position)) {
		agent.type = type;
		agent.position = position;
		agent.sequence = sequence;
		ListStructQWave_push_back(structQWaves, StructQWave_allocinit(position, sequence));
		agent.currEmission = ListStructQWave_at_tail(structQWaves)->value;
		ListACell_push_back(agents, agent);
	} else {
		fprintf(stderr, "Matrix error: cannot ADD new agent at %zu, %zu because indices out of matrix range.\n", position.i, position.j);
	}
	return ! positionExist;
}

#define MAX_LINE_SIZE 45 //counting the \n\0 chars at the end.

static bool parse_and_typecheck_line(char * const lineBuff, ListACell * const agents, ListStructQWave * const structQWaves, const char * const agentsLayoutPath, const size_t lineCounter, const size_t m, const size_t n)
{
	bool exitStatus;
	size_t i, j;
	char * currentField, * end, agentType;
	exitStatus = true;
	if (lineBuff[strlen(lineBuff) - 1] == '\n') {
		currentField = strtok(lineBuff, ";\n");
		if (currentField != NULL) {
			if ((strlen(currentField) == 1) && (! Sequence_are_null(sequenceTable[agentType = currentField[0]]))) {
				currentField = strtok(NULL, ";\n");
				if (currentField != NULL) {
					i = strtoul(currentField, &end, 10);
					if ((isdigit(currentField[0]) != 0) && (*end == '\0') && (errno != ERANGE)) {
						currentField = strtok(NULL, ";\n");
						if (currentField != NULL) {
							j = strtoul(currentField, &end, 10);
							if ((isdigit(currentField[0]) != 0) && (*end == '\0') && (errno != ERANGE)) {
								currentField = strtok(NULL, ";\n");
								if (currentField == NULL) {
									exitStatus = ListACell_add_agent(agents, structQWaves, agentType, Index_get(i, j), sequenceTable[agentType], m, n);
								} else {
									fprintf(stderr, "Error in '%s' input file: line '%lu' has too many fields.\n", agentsLayoutPath, lineCounter);
								}
							} else {
								fprintf(stderr, "Error in '%s' input file: line '%lu', given j index is not a valid unsigned long integer.\n", agentsLayoutPath, lineCounter);
							}
						} else {
							fprintf(stderr, "Error in '%s' input file: line '%lu', missing j index.\n", agentsLayoutPath, lineCounter);
						}
					} else {
						fprintf(stderr, "Error in '%s' input file: line '%lu', given i index is not a valid unsigned long integer.\n", agentsLayoutPath, lineCounter);
					}
				} else {
					fprintf(stderr, "Error in '%s' input file: line '%lu', missing i index.\n", agentsLayoutPath, lineCounter);
				}
			} else {
				fprintf(stderr, "Error in '%s' input file: line '%lu', given agent type does not exist.\n", agentsLayoutPath, lineCounter);
			}
		} else {
			fprintf(stderr, "Error in '%s' input file: line '%lu', missing agent type.\n", agentsLayoutPath, lineCounter);
		}
	} else {
		fprintf(stderr, "Error in '%s' input file: line %lu is too long.\n", agentsLayoutPath, lineCounter);
	}
	return exitStatus;
}

bool ListACell_init(ListACell * const agents, ListStructQWave * const structQWaves, const char * const agentsLayoutPath, const size_t m, const size_t n)
{
	bool exitStatus, subExitStatus;
	char lineBuff[MAX_LINE_SIZE];
	size_t lineCounter;
	FILE * file;
	exitStatus = true;
	if (file = fopen(agentsLayoutPath, "r")) {
		subExitStatus = false;
		lineCounter = 0;
		while (! ((fgets(lineBuff, MAX_LINE_SIZE, file) == NULL) || subExitStatus)) {
			subExitStatus = parse_and_typecheck_line(lineBuff, agents, structQWaves, agentsLayoutPath, lineCounter, m, n);
			lineCounter++;
		}
		if (fclose(file) != EOF) {
			if (lineCounter > 0) {
				exitStatus = subExitStatus;
			} else {
				fprintf(stderr, "Error in '%s' file input: file is empty.\n", agentsLayoutPath);
			}
		} else {
			fprintf(stderr, "Error in '%s' input file: cannot INIT agents due to : %s.\n", agentsLayoutPath, strerror(errno));
		}
	} else {
		fprintf(stderr, "Matrix error: cannot read file '%s' due to : %s.\n", agentsLayoutPath, strerror(errno));
	}
	return exitStatus;
}

bool ACell_change_agent(ACell * const agent, ListStructQWave * const structQWaves, const ACell newAgent, const size_t m, const size_t n)
{
	bool newAgentExist;
	Index newPosition;
	unsigned char newType;
	Sequence newSequence;
	newType = newAgent.type;
	newPosition = newAgent.position;
	newSequence = newAgent.sequence;
	newAgentExist = false;
	if (! Sequence_are_null(sequenceTable[newType])) {
		if (Sequence_are_equal(sequenceTable[newType], newSequence)) {
			if (is_position_in_field(m, n, newPosition)) {
				if ((! Index_are_equal(agent->position, newPosition)) || (! Sequence_are_equal(agent->sequence, newSequence))) {
					agent->currEmission->isEmitted = false;
					ListStructQWave_push_back(structQWaves, StructQWave_allocinit(newPosition, newSequence));
					agent->currEmission = ListStructQWave_at_tail(structQWaves)->value;
				}
				agent->type = newType;
				agent->position = newPosition;
				agent->sequence = newSequence;
				newAgentExist = true;
			} else {
				fprintf(stderr, "Matrix error: cannot MOVE agent to %zu, %zu because indices out of matrix range.\n", newPosition.i, newPosition.j);
			}
		} else {
			fprintf(stderr, "Matrix error: cannot CHANGE agent SEQUENCES because given type '%c' does not match the given sequences.\n", newType);
		}
	} else {
		fprintf(stderr, "Matrix error: cannot CAST agent to %c type because this type does not exist.\n", newType);
	}
	return newAgentExist;
}

LIST_AT_HEAD(ACell, ACell)

LISTELEMENT_ISTAILSENTINEL(ACell, ACell)

LISTELEMENT_GET(ACell, ACell)

bool ListACell_refresh(ListACell * const agents, ListStructQWave * const structQWaves, const VField field)
{
	bool exitStatus;
	ListElementACell * currentListElementACell;
	ACell currentNewAgent;
	exitStatus = false;
	currentListElementACell = ListACell_at_head(agents);
	while (! (ListElementACell_istailsentinel(currentListElementACell) || exitStatus)) {
		currentNewAgent = ACell_next_agent(ListElementACell_get(currentListElementACell), field);
		exitStatus = ! ACell_change_agent(currentListElementACell->value, structQWaves, currentNewAgent, field.m, field.n);
		currentListElementACell = currentListElementACell->next;
	}
	return exitStatus;
}

void ListACell_print_agents_status(ListACell * const agents, FILE * const output)
{
	ListElementACell * currentListElementACell;
	ACell currentAgent;
	size_t agentNumber;
	currentListElementACell = ListACell_at_head(agents);
	agentNumber = 0;
	fputs("[AGENTS]\n", output);
	while (! ListElementACell_istailsentinel(currentListElementACell)) {
		currentAgent = ListElementACell_get(currentListElementACell);
		fprintf(output, "%lu %c %lu,%lu\n", agentNumber, currentAgent.type, currentAgent.position.i, currentAgent.position.j);
		currentListElementACell = currentListElementACell->next;
		agentNumber++;
	}
}

/* CAUWAD stands for Cellular Automata Utilizing WAve Dissipation.
 * Copyright (C) 2021  Sangsoic
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "velocity.h"

VCell VCell_get_initialized(void)
{
	VCell cell;
	cell.x = 0;
	cell.y = 0;
	return cell;
}

VCell VCell_get(const Vtype y, const Vtype x)
{
	VCell cell;
	cell.x = x;
	cell.y = y;
	return cell;
}

VECTOR_SET(VCell, VCell)

void Field_set_all_to_zero(const VField field)
{
	size_t i;
	for (i = 0; i < field.field->capacity; i++) {
		VectorVCell_set(field.field, i, VCell_get_initialized());
	}
}

#define MAX_FIRST_LINE 43 //counting the \n\0 chars at the end.

bool Field_import_field_dimension(const char * const fieldLayoutPath, FILE * * const layoutFile, size_t * const pm, size_t * const pn)
{
	bool exitStatus;
	char lineBuff[MAX_FIRST_LINE], * currentField, * end;
	size_t m, n;
	FILE * file;
	exitStatus = true;
	if (file = fopen(fieldLayoutPath, "r")) {
		*layoutFile = file;
		if (fgets(lineBuff, MAX_FIRST_LINE, file)) {
			if (lineBuff[strlen(lineBuff) - 1] == '\n') {
				currentField = strtok(lineBuff, ";\n");
				if (currentField != NULL) {
					m = strtoul(currentField, &end, 10);
					if ((isdigit(currentField[0]) != 0) && (*end == '\0') && (errno != ERANGE)) {
						currentField = strtok(NULL, ";\n");
						if (currentField != NULL) {
							n = strtoul(currentField, &end, 10);
							if ((isdigit(currentField[0]) != 0) && (*end == '\0') && (errno != ERANGE)) {
								currentField = strtok(NULL, ";\n");
								if (currentField == NULL) {
									if ((m != 0) && (n != 0)) {
										*pm = m;
										*pn = n;
										exitStatus = false;
									} else {
										fprintf(stderr, "Error in '%s' input file: line 0, %c cannot be zero.\n", fieldLayoutPath, m == 0 ? 'm' : 'n');
									}
								} else {
									fprintf(stderr, "Error in '%s' input file: line 0 has too many fields.\n", fieldLayoutPath);
								}
							} else {
								fprintf(stderr, "Error in '%s' input file: line 0, given n dimension is not a valid unsigned long integer.\n", fieldLayoutPath);
							}
						} else {
							fprintf(stderr, "Error in '%s' input file: line 0, missing n dimension.\n", fieldLayoutPath);
						}
					} else {
						fprintf(stderr, "Error in '%s' input file: line 0, given m dimension is not a valid unsigned long integer.\n", fieldLayoutPath);
					}
				} else {
					fprintf(stderr, "Error in '%s' input file: line 0, missing m dimension.\n", fieldLayoutPath);
				}
			} else {
				fprintf(stderr, "Error in '%s' input file: line 0 is too long.\n", fieldLayoutPath);
			}
		} else {
			fprintf(stderr, "Error in '%s' file input: file is empty.\n", fieldLayoutPath);
		}
	} else {
		fprintf(stderr, "Matrix error: cannot read file '%s' due to : %s.\n", fieldLayoutPath, strerror(errno));
	}
	return exitStatus;
}

static bool is_str_valid_double(const char * const str, const char endRef)
{
	return ((isdigit(str[0]) != 0) ||
			((strlen(str) > 1) && (str[0] == '-') && (isdigit(str[1]) != 0))) &&
			(endRef == '\0') && (errno != ERANGE);
}

static bool parse_and_typecheck_line(char * const lineBuff, const VField field, const char * const fieldLayoutPath, const size_t lineCounter, const size_t n)
{
	bool exitStatus, subExitStatus;
	char * currentField, * currentSubField, * end;
	size_t j, savedLength;
	Vtype y, x;
	exitStatus = true;
	currentField = strtok(lineBuff, ";\n");
	j = 0;
	subExitStatus = false;
	while (! (subExitStatus || (currentField == NULL) || (j >= n))) {
		subExitStatus = true;
		savedLength = strlen(currentField);
		currentSubField = strtok(currentField, ",");
		if (currentSubField != NULL) {
			y = strtod(currentSubField, &end);
			if (is_str_valid_double(currentSubField, *end)) {
				currentSubField = strtok(NULL, "\0");
				if (currentSubField != NULL) {
					x = strtod(currentSubField, &end);
					if (is_str_valid_double(currentSubField, *end)) {
						VectorVCell_set(field.field, lineCounter * n + j, VCell_get(y, x));
						subExitStatus = false;
					} else {
						fprintf(stderr, "Error in '%s' file input: line %lu, vector element number %lu has invalid x coordinate (must be a decimal number).\n", fieldLayoutPath, lineCounter+1, j);
					}
				} else {
					fprintf(stderr, "Error in '%s' file input: line %lu, vector element number %lu has no x coordinate.\n", fieldLayoutPath, lineCounter+1, j);
				}
			} else {
				fprintf(stderr, "Error in '%s' file input: line %lu, vector element number %lu has invalid y coordinate (must be a decimal number).\n", fieldLayoutPath, lineCounter+1, j);
			}
		} else {
			fprintf(stderr, "Error in '%s' file input: line %lu, vector element number %lu is empty.\n", fieldLayoutPath, lineCounter+1, j);
		}
		currentField = strtok(currentField + savedLength + 1, ";\n");
		j++;
	}
	if (currentField == NULL) {
		if (j == n) {
			exitStatus = subExitStatus;
		} else {
			fprintf(stderr, "Error in '%s' file input: line %lu has too few elements, expecting line of %lu elements.\n", fieldLayoutPath, lineCounter+1, n);
		}
	} else if (j == n) {
		fprintf(stderr, "Error in '%s' file input: line %lu has too many elements, expecting line of %lu elements.\n", fieldLayoutPath, lineCounter+1, n);
	}
	return exitStatus;
}

bool Field_import_field_from(const VField field, const char * const fieldLayoutPath, FILE * file)
{
	bool exitStatus, subExitStatus, noExtraLine;
	char * lineBuff;
	size_t i, j, m, n, lineBuffSize;
	exitStatus = true;
	lineBuff = NULL;
	i = 0;
	j = 0;
	m = field.m;
	n = field.n;
	lineBuffSize = 0;
	subExitStatus = false;
	while (! (subExitStatus || (getline(&lineBuff, &lineBuffSize, file) == -1) || (i >= m))) {
		subExitStatus = parse_and_typecheck_line(lineBuff, field, fieldLayoutPath, i, n);
		i++;
	}
	noExtraLine = getline(&lineBuff, &lineBuffSize, file) == -1;
	if (fclose(file) != EOF) {
		if (noExtraLine) {
			if (i == m) {
				exitStatus = subExitStatus;
			} else {
				fprintf(stderr, "Error in '%s' input file: too few lines, expecting an %lu by %lu matrix.\n", fieldLayoutPath, m, n);
			}
		} else if (i == m){
			fprintf(stderr, "Error in '%s' input file: too many lines.\n", fieldLayoutPath);
		}
	} else {
		fprintf(stderr, "Error in '%s' input file: cannot close file due to : %s.\n", fieldLayoutPath, strerror(errno));
	}
	free(lineBuff);
	return exitStatus;
}

VECTOR_GET(VCell, VCell)

void Field_print_x_field(const VField field, FILE * const output, const unsigned char precision)
{
	size_t i, j, m, n, o;
	VectorVCell * vfield;
	vfield = field.field;
	m = field.m;
	n = field.n;
	o = field.n - 1;
	fputs("[FIELD]\n", output);
	for (i = 0; i < m; i++) {
		for (j = 0; j < o; j++) {
			fprintf(output, "%.*lf ", precision, VectorVCell_get(vfield, i * n + j).x);
		}
		fprintf(output, "%.*lf\n", precision, VectorVCell_get(vfield, i * n + j).x);
	}
}

void Field_print_y_field(const VField field, FILE * const output, const unsigned char precision)
{
	size_t i, j, m, n, o;
	VectorVCell * vfield;
	vfield = field.field;
	m = field.m;
	n = field.n;
	o = field.n - 1;
	fputs("[FIELD]\n", output);
	for (i = 0; i < m; i++) {
		for (j = 0; j < o; j++) {
			fprintf(output, "%.*lf ", precision, VectorVCell_get(vfield, i * n + j).y);
		}
		fprintf(output, "%.*lf\n", precision, VectorVCell_get(vfield, i * n + j).y);
	}
}

void Field_print_yx_field(const VField field, FILE * const output, const unsigned char precision)
{
	size_t i, j, m, n, o;
	VectorVCell * vfield;
	VCell vcell;
	vfield = field.field;
	m = field.m;
	n = field.n;
	o = field.n - 1;
	fputs("[FIELD]\n", output);
	for (i = 0; i < m; i++) {
		for (j = 0; j < o; j++) {
			vcell = VectorVCell_get(vfield, i * n + j);
			fprintf(output, "%.*lf,%.*lf ", precision, vcell.y, precision, vcell.x);
		}
		vcell = VectorVCell_get(vfield, i * n + j);
		fprintf(output, "%.*lf,%.*lf\n", precision, vcell.y, precision, vcell.x);
	}
}

void Field_apply_reset(const VField field, const size_t linearIndices, const Index rectangularIndices, const Wave wave)
{
	VectorVCell_set(field.field, linearIndices, VCell_get_initialized());
}

size_t ij_linear_indices(const size_t i, const size_t j, const size_t n)
{
	return i * n + j;
}

size_t ji_linear_indices(const size_t i, const size_t j, const size_t n)
{
	return j * n + i;
}

Index ij_rectangular_indices(const size_t i, const size_t j)
{
	return Index_get(i, j);
}

Index ji_rectangular_indices(const size_t i, const size_t j)
{
	return Index_get(j, i);
}

static void Field_plot_wave_top_left(const VField field, const Wave wave, const ssize_t lowerbound, const size_t n, size_t (* const linear_indices)(const size_t, const size_t, const size_t), Index (* rectangular_indices)(const size_t, const size_t), void (* action)(const VField, const size_t, const Index, const Wave))
{
	ssize_t upperbound, j, i;
	size_t linearIndices;
	Index rectangularIndices;
	upperbound = wave.center.j;
	if (wave.radius > (size_t)wave.center.i) {
		upperbound = Wave_top_left_boundary(wave);
	}
	for (j = lowerbound; j <= upperbound; j++) {
		i = Wave_top_wave(j, wave);
		linearIndices = linear_indices(i, j, n);
		rectangularIndices = rectangular_indices(i, j);
		action(field, linearIndices, rectangularIndices, wave);
	}
}

static void Field_plot_wave_bot_left(const VField field, const Wave wave, const ssize_t lowerbound, const size_t n, size_t (* const linear_indices)(const size_t, const size_t, const size_t), Index (* rectangular_indices)(const size_t, const size_t), void (* action)(const VField, const size_t, const Index, const Wave))
{
	ssize_t upperbound, j, i;
	size_t linearIndices;
	Index rectangularIndices;
	upperbound = wave.center.j;
	if ((ssize_t)wave.radius > (j = abs(field.m - 1 - wave.center.i))) {
		upperbound = Wave_bot_left_boundary(wave, j);
	}
	for (j = lowerbound; j <= upperbound; j++) {
		i = Wave_bot_wave(j, wave);
		linearIndices = linear_indices(i, j, n);
		rectangularIndices = rectangular_indices(i, j);
		action(field, linearIndices, rectangularIndices, wave);
	}
}

static void Field_plot_wave_top_right(const VField field, const Wave wave, const ssize_t upperbound, const size_t n, size_t (* const linear_indices)(const size_t, const size_t, const size_t), Index (* rectangular_indices)(const size_t, const size_t), void (* action)(const VField, const size_t, const Index, const Wave))
{
	ssize_t lowerbound, j, i;
	size_t linearIndices;
	Index rectangularIndices;
	lowerbound = wave.center.j + 1;
	if (wave.radius > (size_t)wave.center.i) {
		lowerbound = Wave_top_right_boundary(wave);
	}
	for (j = upperbound; j >= lowerbound; j--) {
		i = Wave_top_wave(j, wave);
		linearIndices = linear_indices(i, j, n);
		rectangularIndices = rectangular_indices(i, j);
		action(field, linearIndices, rectangularIndices, wave);
	}
}

static void Field_plot_wave_bot_right(const VField field, const Wave wave, const ssize_t upperbound, const size_t n, size_t (* const linear_indices)(const size_t, const size_t, const size_t), Index (* rectangular_indices)(const size_t, const size_t), void (* action)(const VField, const size_t, const Index, const Wave))
{
	ssize_t lowerbound, j, i;
	size_t linearIndices;
	Index rectangularIndices;
	lowerbound = wave.center.j + 1;
	if ((ssize_t)wave.radius > (j = abs(field.m - 1 - wave.center.i))) {
		lowerbound = Wave_bot_right_boundary(wave, j);
	}
	for (j = upperbound; j >= lowerbound; j--) {
		i = Wave_bot_wave(j, wave);
		linearIndices = linear_indices(i, j, n);
		rectangularIndices = rectangular_indices(i, j);
		action(field, linearIndices, rectangularIndices, wave);
	}
}

static void Field_plot_half_wave(const VField field, const Wave wave, const size_t n, const unsigned int gapsbound, size_t (* const linear_indices)(const size_t, const size_t, const size_t), Index (* rectangular_indices)(const size_t, const size_t), void (* action)(const VField, const size_t, const Index, const Wave))
{
	ssize_t lowerbound, upperbound;
	lowerbound = wave.center.j - gapsbound;
	if (lowerbound < 0) {
		lowerbound = 0;
	}
	Field_plot_wave_top_left(field, wave, lowerbound, n, linear_indices, rectangular_indices, action);
	Field_plot_wave_bot_left(field, wave, lowerbound, n, linear_indices, rectangular_indices, action);
	upperbound = wave.center.j + gapsbound;
	if (upperbound >= (ssize_t)field.n) {
		upperbound = field.n - 1;
	}
	Field_plot_wave_top_right(field, wave, upperbound, n, linear_indices, rectangular_indices, action);
	Field_plot_wave_bot_right(field, wave, upperbound, n, linear_indices, rectangular_indices, action);
}

void Field_plot_wave(VField field, Wave wave, void (* action)(const VField, const size_t, const Index, const Wave))
{
	unsigned int gapsbound;
	size_t tmp;
	if (wave.radius != 0) {
		gapsbound = Wave_gaps_boundary(wave.radius);
		Field_plot_half_wave(field, wave, field.n, gapsbound, ij_linear_indices, ij_rectangular_indices, action);
		gapsbound = wave.center.i - (Wave_top_wave(wave.center.j + gapsbound, wave) + 1);
		wave.center = Index_switch(wave.center);
		tmp = field.m;
		field.m = field.n;
		field.n = tmp; 
		Field_plot_half_wave(field, wave, field.m, gapsbound, ji_linear_indices, ji_rectangular_indices, action);
	} else {
		action(field, ij_linear_indices(wave.center.i, wave.center.j, field.n), ij_rectangular_indices(wave.center.i, wave.center.j), wave);
	}
}

QUEUE_AT_HEAD(Wave, Wave)

static void Field_plot_qwave(const VField field, QueueWave * const waves, void (* action)(const VField, const size_t, const Index, const Wave))
{
	QueueElementWave * currentWave;
	currentWave = QueueWave_at_head(waves);
	while (currentWave != NULL) {
		Field_plot_wave(field, *currentWave->value, action);
		currentWave = currentWave->next;
	}
}

LIST_AT_HEAD(struct QueueWave, StructQWave)

LISTELEMENT_ISTAILSENTINEL(struct QueueWave, StructQWave)

LISTELEMENT_GET(struct QueueWave, StructQWave)

void Field_plot_qwaves(const VField field, ListStructQWave * const qwaveList, const Action action)
{
	ListElementStructQWave * qwaveListElement;
	void (* act)(const VField, const size_t, const Index, const Wave);
	switch (action) {
		case PLOT :
			act = Field_apply_wave;
			break;
		case RESET :
			act = Field_apply_reset;
			break;
	}
	qwaveListElement = ListStructQWave_at_head(qwaveList);
	while (! ListElementStructQWave_istailsentinel(qwaveListElement)) {
		Field_plot_qwave(field, ListElementStructQWave_get(qwaveListElement).waves, act);
		qwaveListElement = qwaveListElement->next;
	}
}

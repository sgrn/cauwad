/* CAUWAD stands for Cellular Automata Utilizing WAve Dissipation.
 * Copyright (C) 2021  Sangsoic
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef INDEX_H
	#define INDEX_H

	#include <stdlib.h>
	#include <stdbool.h>

	struct Index
	{
		size_t i;
		size_t j;
	};

	typedef struct Index Index;
	

	/**
	 * \fn Index Index_get(const size_t i, const size_t j)
	 * \brief Gets \a Index object of given coordinate.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 */
	Index Index_get(const size_t i, const size_t j);

	/**
	 * \fn Index Index_switch(const Index index)
	 * \brief Switches indices.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 */
	Index Index_switch(const Index index);

	/**
	 * \fn bool Index_are_equal(const Index a, const Index b)
	 * \brief Tests if index are equals.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 */
	bool Index_are_equal(const Index a, const Index b);

#endif

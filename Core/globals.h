/* CAUWAD stands for Cellular Automata Utilizing WAve Dissipation.
 * Copyright (C) 2021  Sangsoic
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef GLOBALS_H
	#define GLOBALS_H

	#include <stddef.h>
	#include <limits.h>

	#include "wave.h"
	#include "index.h"

	#define SEQUENCE_TABLE_SIZE UCHAR_MAX

	extern unsigned long long globalInterationNumber;

	extern Sequence sequenceTable[SEQUENCE_TABLE_SIZE];

#endif

/* CAUWAD stands for Cellular Automata Utilizing WAve Dissipation.
 * Copyright (C) 2021  Sangsoic
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef VELOCITY_H
	#define VELOCITY_H

	#include <stdlib.h>
	#include <stdio.h>
	#include <errno.h>
	#include <ctype.h>
	#include <stdbool.h>

	#include "wave.h"

	#include "../Lib/Vector/vector.h"
	#include "../Lib/Queue/queue.h"

	enum Action
	{
		PLOT,
		RESET
	};

	typedef enum Action Action;

	struct VelocityCell
	{
		Vtype x;
		Vtype y;
	};

	typedef struct VelocityCell VCell;

	VECTOR(VCell, VCell)

	struct VelocityField
	{
		VectorVCell * field;
		size_t m, n;
	};

	typedef struct VelocityField VField;

	#include "../SharedLib/shared-velocity.h"

	/**
	 * \fn VCell VCell_get_initialized(void)
	 * \brief Gets an initialized.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 */
	VCell VCell_get_initialized(void);

	/**
	 * \fn void Field_print_y_field(const VField field, FILE * const output, const unsigned char precision)
	 * \brief Prints Y coordinates of the vector field.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 */
	void Field_print_y_field(const VField field, FILE * const output, const unsigned char precision);

	/**
	 * \fn void Field_print_x_field(const VField field, FILE * const output, const unsigned char precision)
	 * \brief Prints X coordinates of the vector field.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 */
	void Field_print_x_field(const VField field, FILE * const output, const unsigned char precision);

	/**
	 * \fn void Field_print_yx_field(const VField field, FILE * const ouput, const unsigned char precision)
	 * \brief Prints YX coordinates of the vector field.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 */
	void Field_print_yx_field(const VField field, FILE * const ouput, const unsigned char precision);

	/**
	 * \fn void Field_set_all_to_zero(const VField field)
	 * \brief Sets all vectors in the field to (0, 0).
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 */
	void Field_set_all_to_zero(const VField field);

	/**
	 * \fn bool Field_import_field_dimension(const char * const fieldLayoutPath, FILE * * const layoutFile, size_t * const pm, size_t * const pn)
	 * \brief Import field dimensions from file.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 */
	bool Field_import_field_dimension(const char * const fieldLayoutPath, FILE * * const layoutFile, size_t * const pm, size_t * const pn);

	/**
	 * \fn bool Field_import_field_from(const VField field, const char * const fieldLayoutPath, FILE * file)
	 * \brief Import field from file.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 */
	bool Field_import_field_from(const VField field, const char * const fieldLayoutPath, FILE * file);

	/**
	 * \fn void Field_plot_wave(VField field, Wave wave, void (* action)(const VField, const size_t, const Index, const Wave))
	 * \brief Plots wave.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 */
	void Field_plot_wave(VField field, Wave wave, void (* action)(const VField, const size_t, const Index, const Wave));

	/**
	 * \fn void Field_plot_qwaves(const VField field, ListStructQWave * const qwaveList, const Action action)
	 * \brief Plots queue waves.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 */
	void Field_plot_qwaves(const VField field, ListStructQWave * const qwaveList, const Action action);
#endif
